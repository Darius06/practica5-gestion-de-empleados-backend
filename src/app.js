const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

//configurar cors
app.use(cors({origin: 'http://localhost:4200'}));

// Middlewares
app.use(morgan('dev')); // Configuración de morgan antes de express.json()
app.use(express.json());

// Settings
app.set('puerto', process.env.PORT || 3000);
app.set('nombreApp', 'Gestión de empleados');

// Routes
app.use('/api/empleados', require('./routes/empleados.routes'));


module.exports = app;
