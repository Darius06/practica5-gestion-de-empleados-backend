const app = require('../app');
const request = require('supertest');

describe('Endpoint de Empleados', () => {
    it('Obtener una lista de empleados existentes', (done) => {
        request(app)
          .get('/api/empleados')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done);
          done();
      });

      it('Editar un empleado existente correctamente', (done) => {
        const idEmpleadoExistente = '651cd06d54ac778d54162f11';
    
        const datosEdicion = {
          nombre: 'Nuevo Test',
          cargo: 'Nuevo Test',
          departamento: 'Ventas',
          sueldo: 600,
        };
    
        request(app)
          .put(`/api/empleados/${idEmpleadoExistente}`)
          .set('Accept', 'application/json')
          .send(datosEdicion)
          .expect('Content-Type', /json/)
          .expect(200, done);
          done();
      });
    
      it('Editar la edición de un empleado que no existe', (done) => {
        const idEmpleadoNoExiste = '651cd06d54ac778d54162faa';
    
        const datosEdicion = {
          nombre: 'Nuevo Test',
          cargo: 'Nuevo Test',
          departamento: 'Ventas',
          sueldo: 600,
        };
    
        request(app)
          .put(`/api/empleados/${idEmpleadoNoExiste}`)
          .set('Accept', 'application/json')
          .send(datosEdicion)
          .expect('Content-Type', /json/)
          .expect(404, done); 
          done();
      });

    
      it('Eliminar un empleado inexistente', (done) => {
        const idEmpleadoInexistente = '651cd06d54ac778d54162faa';
    
        request(app)
          .delete(`/api/empleados/${idEmpleadoInexistente}`)
          .expect(404, done);
          done();
      });
});