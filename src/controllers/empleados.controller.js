const empleadoCtrl={};
const Empleado=require('../models/Empleado');



/*
empleadoCtrl.getEmpleados=(req,res)=>{
    res.send('get empleados')
}
empleadoCtrl.createEmpleado=(req,res)=>{
    res.send('Se creo el empleado')
}
empleadoCtrl.getEmpleado=(req,res)=>{
    //res.send('get empleados')
}
empleadoCtrl.editEmpleado=(req,res)=>{
    //res.send('get empleados')
}
empleadoCtrl.deleteEmpleado=(req,res)=>{}
*/

//router.get('/',empleadosController.getEmpleados);


empleadoCtrl.getEmpleados=async(req,res)=>
{
    const empleados=await Empleado.find();
    res.json(empleados);

    //res.send('get empleados')
}

empleadoCtrl.createEmpleado= async(req,res)=>{
    //console.log("requeste es: ",req);
    const empleado=new Empleado({
    nombre: req.body.nombre,
    cargo: req.body.cargo,
    departamento:req.body.departamento,
    sueldo:req.body.sueldo
    });
    // Validar los datos antes de guardar
const validationError = empleado.validateSync();
if (validationError) {
  return res.status(400).json({ error: validationError.message });
}
    //console.log(empleado);
    await empleado.save();
    res.json('status: Datos guardados');
   }

empleadoCtrl.editEmpleado=async(req,res)=>{
    //console.log("el id es: ",req.body._id)
    const {_id}=req.body._id;
    const empleado={
    nombre: req.body.nombre,
    cargo: req.body.cargo,
    departamento: req.body.departamento,
    sueldo: req.body.sueldo
    };    
    await Empleado.findByIdAndUpdate(req.body._id, {$set:empleado},{new: true});
    res.json('status: Datos actualizados');    
}

empleadoCtrl.deleteEmpleado=async(req,res)=>{
    await Empleado.findByIdAndRemove(req.params.id);
    res.json('status: Empleado ha sido removido');
}

module.exports=empleadoCtrl;